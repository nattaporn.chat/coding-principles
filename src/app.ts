export function sayHelloWorld(world: string) {
  return `Hello ${world}`;
}

export function hello(word: string) {
  console.log(sayHelloWorld(word))
}

hello('OK')